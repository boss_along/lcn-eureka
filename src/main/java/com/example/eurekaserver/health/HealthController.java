package com.example.eurekaserver.health;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

    @PostMapping("/ping")
    public String health(){
       return "pong";
    }
}
